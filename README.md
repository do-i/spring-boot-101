# spring-boot-101

## build and run
 
### without docker

    ./gradlew build
    java -jar build/libs/spring-boot-proto-101.jar
    
### with docker

Make sure Dockerfile is the current directory
    
    docker build -t spring-boot-101:latest .  
    
    docker run -d -p 8085:8085 --name spring-boot-101 spring-boot-101:latest

Note: `--name` overrides container random name

## Access
On host

    curl localhost:8085

## run on Raspberry Pi

    scp Dockerfile pi@pi4j:
    ssh pi@pi4j 'mkdir -p build/libs'
    scp build/libs/spring-boot-proto-1.0.0.jar pi@pi4j:build/libs/
    
    ssh pi@pi4j
    docker build -t spring-boot-101:latest .
    docker run -d -p 8085:8085 --name spring-boot-101 spring-boot-101:latest

## Access within Pi
It takes about 30 to 60 seconds to complete boot process.
    
    curl localhost:8085
    
## Access from host

    curl pi4j:8085
    
## Install Compose as container (x86_64)

    sudo curl -L --fail https://github.com/docker/compose/releases/download/1.25.0/run.sh -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
Note: with Compose there is no need to run the run command any more 
all the run parameters are configured in `docker-compose.yml`   

### for ARM (RaspberryPi)
       
    sudo apt install -y libffi-dev libssl-dev
    sudo apt install -y python3 python3-pip
    sudo pip3 install docker-compose
    
## Run docker compose via make

    make help

This command is equivalent of running `make stop clean gradle_build docker_build build up`
    
    make local_all
    
To run on remote host

    make remote_all    

    
## Endpoints

    curl localhost:8087
    
    curl localhost:8087/recipe/1

    
## Spring Boot and Postgres

reference: https://medium.com/@isurunuwanthilaka/docker-zero-to-hero-with-springboot-postgres-e0b8c3a4dccb
Add data store layer in the web app and do some database stuff

## TODO k8s

reference: https://medium.com/nycdev/k8s-on-pi-9cc14843d43


## Remote deploy (TODO) 
First check this works by running this. Output should be `raspberrypi`
    
    docker -H "ssh://pi@pi4j" run --rm --net host busybox hostname -f
     