DROP DATABASE IF EXISTS tinydb;
CREATE DATABASE tinydb;
DROP TABLE IF EXISTS Recipe CASCADE;
CREATE TABLE Recipe (
  id SERIAL NOT NULL,
  name VARCHAR(64),
  instructions TEXT,
  PRIMARY KEY (id)
);
INSERT INTO Recipe(id, name, instructions) VALUES
    (1, 'Ramen', 'Make soup, Cook noodles for 3 minutes'),
    (2, 'Sushi', 'Slice fish, put fish on rice, and serve.');