# It is important to use actual tab character for indentations not white spaces.
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

clean: ## gradle clean
	./gradlew clean

gradle_build: ## gradle build
	./gradlew build

docker_build: ## docker build
	docker build -t spring-boot-101 .

build: ## docker-compose build
	docker-compose build

up: ## This starts all containers
	docker-compose up -d

stop: ## This stops all containers and remove them
	docker-compose down

scp_assets: ## SCP assets to remote server
	scp Dockerfile pi@pi4j:
	scp Makefile pi@pi4j:
	scp docker-compose.yml pi@pi4j:
	ssh pi@pi4j 'mkdir -p build/libs'
	scp build/libs/spring-boot-proto-1.0.0.jar pi@pi4j:build/libs/

local_all: stop clean gradle_build docker_build build up

remote_stop:
	ssh pi@pi4j "make stop"

remote_all: scp_assets
	ssh pi@pi4j "make stop docker_build build up"
	echo "To access run: curl pi4j:8087"



