FROM adoptopenjdk:11.0.5_10-jdk-hotspot
RUN mkdir /opt/app
COPY build/libs/spring-boot-proto-1.0.0.jar /opt/app/app.jar
CMD ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=0.0.0.0:5081", "-jar", "-Dserver.port=8085", "/opt/app/app.jar"]