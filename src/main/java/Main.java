public class Main {

  /**
   * Just to prove that we can have two main methods and spring boot still works by specifying
   *
   * <pre>
   * springBoot {
   *     mainClassName = 'com.djd.fun.springboot101.Main'
   * }
   * </pre>
   *
   * @param args
   */
  public static void main(String[] args) {}
}
