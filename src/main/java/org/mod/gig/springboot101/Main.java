package org.mod.gig.springboot101;

import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Default server.port is 8080 application.yml overrides it with 8081 system property
 * (-Dserver.port=8085) overrides application yml arguments (--server.port=8086) overrides system
 * prop
 */
@Slf4j
public class Main {

  // no instance allowed
  private Main() {}

  public static void main(String[] args) {

    ConfigurableApplicationContext applicationContext =
        SpringApplication.run(Application.class, args);
    log.info("applicationContext.isActive: " + applicationContext.isActive());
  }

  @Slf4j
  @EntityScan("org.mod.gig.springboot101.entity")
  @EnableJpaRepositories("org.mod.gig.springboot101.repository")
  @SpringBootApplication
  public static class Application {

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
      return args -> {
        log.info("Let's inspect the beans provided by Spring Boot:");
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        log.debug("beanNames: {}", Arrays.asList(beanNames));
        log.info("args: {}", Arrays.asList(args));
      };
    }
  }
}
