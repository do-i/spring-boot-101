package org.mod.gig.springboot101.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Recipe")
@Getter
@Setter
public class RecipeEntity {

  @Id private Long id;
  private String name;
  private String instructions;
}
