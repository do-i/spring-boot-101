package org.mod.gig.springboot101.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

  @Bean("coolName")
  public String provideCoolName() {
    return "cool-Name v5";
  }

  /**
   * consumer of this bean should be like @Qualifier("badName) String badName or use just String
   * badName. As long as variable name is same as name of bean (e.g., @Bean("badName")) SpringBoot
   * does not complain.
   */
  @Bean("badName")
  public String provideBadName() {
    return "bad-Name v7";
  }
}
