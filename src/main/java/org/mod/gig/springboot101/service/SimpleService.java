package org.mod.gig.springboot101.service;

import java.util.Optional;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mod.gig.springboot101.model.Recipe;
import org.mod.gig.springboot101.repository.RecipeRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class SimpleService {

  private final RecipeRepository recipeRepository;
  private final String badName; // @Qualifier("badName")
  private final String coolName; // @Qualifier("coolName")

  @Transactional
  public Optional<Recipe> findRecipe(long id) {
    return recipeRepository
        .findById(id)
        .map(
            recipeEntity ->
                Recipe.builder()
                    .id(recipeEntity.getId())
                    .name(recipeEntity.getName())
                    .instruction(recipeEntity.getInstructions())
                    .build());
  }

  public String doSomethingSimple() {
    String someValue = String.format("%s or %s", coolName, badName);
    log.info(someValue);
    return someValue;
  }
}
