package org.mod.gig.springboot101.model;

import java.util.List;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
public class SlimRecipe {

  String dietPlanName;

  @Singular("instruction")
  List<String> instructions;
}
