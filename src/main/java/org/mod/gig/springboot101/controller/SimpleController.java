package org.mod.gig.springboot101.controller;

import lombok.extern.slf4j.Slf4j;
import org.mod.gig.springboot101.model.Recipe;
import org.mod.gig.springboot101.model.SlimRecipe;
import org.mod.gig.springboot101.service.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @RestController spring boot maps methods in this class to RESTful endpoints @Slf4j compiler
 * generates {@code private static final org.slf4j.Logger log =
 * org.slf4j.LoggerFactory.getLogger(LogExample.class);}
 */
@Slf4j
@RestController
public class SimpleController {

  private final SimpleService simpleService;

  @Autowired
  public SimpleController(SimpleService simpleService) {
    this.simpleService = simpleService;
  }

  @GetMapping("/")
  public String index() {
    String result = String.format("This is amazing. %s", simpleService.doSomethingSimple());
    log.info(result);
    return result;
  }

  @GetMapping("/recipe/{id}")
  public Recipe findRecipe(@PathVariable("id") String id) {
    return simpleService.findRecipe(Long.parseLong(id)).orElseThrow();
  }

  @PostMapping()
  public SlimRecipe create(@RequestBody SlimRecipe slimRecipe) {
    return slimRecipe;
  }
}
