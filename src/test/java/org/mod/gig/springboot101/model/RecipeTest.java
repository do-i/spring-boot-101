package org.mod.gig.springboot101.model;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class RecipeTest {

  private static final Recipe RECIPE =
      Recipe.builder()
          .id(1)
          .name("Teriyaki Salmon")
          .instruction("Make Sauce")
          .instruction("Grill Salmon")
          .build();

  @Test
  void immutable() {
    List<String> mutant = new ArrayList<>();
    mutant.add("Add Soy Sause");
    Recipe mutableRecipe = RECIPE.toBuilder().clearInstructions().instructions(mutant).build();
    assertThat(mutableRecipe.getInstructions()).containsExactly("Add Soy Sause");
    //    Let's mutate source list
    mutant.add("Add pinch of sea salt");
    // Although source list is mutated, recipe's list is immutable
    assertThat(mutableRecipe.getInstructions()).containsExactly("Add Soy Sause");
  }

  @Test
  void json() throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    String serializedJson = objectMapper.writeValueAsString(RECIPE);
    assertThat(serializedJson)
        .isEqualTo(
            "{\"id\":1,\"instructions\":[\"Make Sauce\",\"Grill Salmon\"],\"customName\":\"Teriyaki Salmon\"}");
    assertThat(objectMapper.readValue(serializedJson, Recipe.class)).isEqualTo(RECIPE);
  }
}
