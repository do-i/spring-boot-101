package org.mod.gig.springboot101.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mod.gig.springboot101.service.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class SimpleControllerTest {

  @MockBean private SimpleService simpleService;
  @Autowired private MockMvc mvc;

  @Test
  void index() throws Exception {
    when(simpleService.doSomethingSimple()).thenReturn("Good Service");
    mvc.perform(get("/"))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("This is amazing. Good Service")));
  }
}
