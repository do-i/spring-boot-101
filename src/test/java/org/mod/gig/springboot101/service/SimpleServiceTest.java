package org.mod.gig.springboot101.service;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mod.gig.springboot101.repository.RecipeRepository;

@ExtendWith(MockitoExtension.class)
class SimpleServiceTest extends Mockito {

  private @Mock RecipeRepository recipeRepository;
  private SimpleService service;

  @BeforeEach
  void setUp() {
    service = new SimpleService(recipeRepository, "BAD NAME", "Cool Name");
  }

  @Test
  void doSomethingSimple() {
    assertThat(service.doSomethingSimple()).isEqualTo("Cool Name or BAD NAME");
  }
}
